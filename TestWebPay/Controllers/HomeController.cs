﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Microsoft.Web.Administration;
using System.IdentityModel.Protocols.WSTrust;

namespace TestWebPay.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.commerceId = "597020000040";
            ViewBag.buyOrder = "12345";
            ViewBag.sessionId = "12334466";
            ViewBag.returnUrl = Url.Action("Resultado","Home");
            ViewBag.finalUrl = Url.Action("Final", "Home");
            ViewBag.commerceCode = "597020000040"; //'id-comercio';
            ViewBag.amount = "20000";
            ViewBag.shareNumber = "";
            ViewBag.shareAmount = "";

            return View();
        }

        public ActionResult Test(FormCollection form)
        {
            var wsInitTransactionInput = new webPayService.wsInitTransactionInput();
            var wsTransactionDetail = new webPayService.wsTransactionDetail();

            wsInitTransactionInput.wSTransactionType = webPayService.wsTransactionType.TR_NORMAL_WS;

            wsInitTransactionInput.sessionId = form["sessionId"].ToString();
            wsInitTransactionInput.buyOrder = form["buyOrder"].ToString();
            wsInitTransactionInput.commerceId = form["commerceId"].ToString();
            wsInitTransactionInput.returnURL = form["returnUrl"].ToString();
            wsInitTransactionInput.finalURL = form["finalUrl"].ToString();

            wsTransactionDetail.commerceCode = form["commerceId"].ToString();
            wsTransactionDetail.buyOrder = form["buyOrder"].ToString();
            wsTransactionDetail.amount = decimal.Parse(form["amount"].ToString());
            wsTransactionDetail.sharesNumber = 0;
            wsTransactionDetail.sharesAmount = 0;

            wsInitTransactionInput.transactionDetails = new webPayService.wsTransactionDetail[] { wsTransactionDetail };

            string url = "https://webpay3gint.transbank.cl/WSWebpayTransaction/cxf/WSWebpayService";
            string configurationName = @"C:\Users\mariol\Downloads\files_test\cumbregroup.crt";

            webPayService.WSWebpayServiceClient proxy = new webPayService.WSWebpayServiceClient(configurationName, url);
            var result = proxy.initTransaction(wsInitTransactionInput);

            ViewBag.Token = result.token;
            ViewBag.Url = result.url;

            return View();
        }
		
        public ActionResult Resultado()
        {
            return View();
        }
		
        public ActionResult Final()
        {
            return View();
        }
    }
}